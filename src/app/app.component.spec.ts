import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('AppComponent', () => {
  // Declaramos las variables que vamos a usar
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent, TodoListComponent],
      imports: [
        HttpClientModule,
        FormsModule,
        RouterTestingModule,
        ReactiveFormsModule,
      ],
    }).compileComponents();

    // Inicializamos la variables fixture y app antes de cada test
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  });

  it('debe crear la aplicación', () => {
    expect(app).toBeTruthy();
  });

  it(`Debe tener como título 'proyecto-testing'`, () => {
    expect(app.title).toEqual('proyecto-testing');
  });
});
