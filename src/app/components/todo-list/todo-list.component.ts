import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Todo } from 'src/models/Todo';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
})
export class TodoListComponent implements OnInit {
  todos: Todo[] = [];
  todoForm: FormGroup;

  constructor(public data: DataService, private formBuilder: FormBuilder) {}

  deleteTodo(todo: Todo) {
    const index = this.todos.findIndex(
      (element: Todo) => element.id === todo.id
    );
    this.todos.splice(index, 1);
  }

  createTodo() {
    if (this.todoForm.invalid) return;
    const { id, user_id, title, due_on } = this.todoForm.value;
    const todo = new Todo(
      Number(id),
      Number(user_id),
      title,
      due_on,
      'pending'
    );
    this.todos.push(todo);
  }

  ngOnInit(): void {
    this.todoForm = this.formBuilder.group({
      id: ['', Validators.required],
      user_id: ['', Validators.required],
      title: ['', Validators.required],
      due_on: ['', Validators.required],
    });

    this.data.getTodos().subscribe(
      (response: any) => (this.todos = response.data),
      (error: any) =>
        console.log(
          `Ha habido un error al obtener la lista de tareas: ${error}`
        )
    );
  }
}
