import { HttpClientModule } from '@angular/common/http';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

// Imports propios
import { TodoListComponent } from './todo-list.component';
import { DataService } from 'src/app/services/data.service';
import { TodoServiceStub } from 'src/app/services/mocks/data.service.mock';
import { By } from '@angular/platform-browser';

describe('Pruebas para el componente TodoListComponent', () => {
  let component: TodoListComponent;
  let fixture: ComponentFixture<TodoListComponent>;
  let formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TodoListComponent],
      imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      providers: [
        {
          provide: DataService,
          useClass: TodoServiceStub, // Le decimos, que donde el componente use AlumnosService, realmente use AlumnosServiceStub
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy(); //  aquí comprueba que no sea undefined
  });

  it('Debería obtener del servicio la lista de tareas y guardarla', () => {
    fixture.whenStable().then(() => {
      expect(component.todos.length).toBeGreaterThan(0);
    });
  });

  it('Debe devolver formulario invalido', () => {
    const app = fixture.componentInstance;
    fixture.detectChanges();

    const todoForm = app.todoForm;
    const id = app.todoForm.controls['id'];
    id.setValue('115');
    expect(todoForm.invalid).toBeTrue();
  });

  it('Debe devolver un formulario valido', () => {
    const app = fixture.componentInstance;
    fixture.detectChanges();

    let id = app.todoForm.controls['id'];
    let user_id = app.todoForm.controls['user_id'];
    let title = app.todoForm.controls['title'];
    let due_on = app.todoForm.controls['due_on'];

    id.setValue('321');
    user_id.setValue('21');
    title.setValue('Id dignissimos adnuo officia adduco.');
    due_on.setValue('2021-09-29T00:00:00.000+05:30');
    expect(app.todoForm.invalid).toBeFalse();
  });

  it('La función createTodo no añade una tarea si el formulario es invalido', () => {
    let cont = component.todos.length;
    component.createTodo();
    expect(component.todos.length).toEqual(cont);
  });

  it('La funcion create añade una tarea', () => {
    let cont = component.todos.length;
    component.todoForm = formBuilder.group({
      id: 0,
      user_id: 256,
      title: 'Non teneo dapifer ars',
      due_on: '2021-10-19T00:00:00.000+05:30',
      status: 'pending',
    });
    component.createTodo();
    expect(component.todos.length).toEqual(cont + 1);
  });

  it(
    'Comprueba si el array aumenta',
    waitForAsync(() => {
      expect(component.todos.length).toBeGreaterThanOrEqual(0);

      component.todos.push({
        id: 526,
        user_id: 54,
        title: 'Appello appositus benigne rem tripudio',
        due_on: '2021-10-11T00:00:00.000+05:30',
        status: 'pending',
      });
      expect(component.todos.length).toBeGreaterThanOrEqual(1);
    })
  );

  it('Comprobar deleteTodo', () => {
    let cont = component.todos.length;

    component.deleteTodo(component.todos[13]);
    expect(component.todos.length).toEqual(cont - 1);
  });

  it('El array despues de llamar al OnInit se rellena', () => {
    component.ngOnInit();
    expect(component.todos.length).toBeGreaterThan(0);
  });

  it('Se debe llamar a getTodos del Servicio en ngOnInit', () => {
    spyOn(component.data, 'getTodos').and.callThrough();
    component.ngOnInit();
    expect(component.data.getTodos).toHaveBeenCalledTimes(1);
  });

  it('El Formulario debe tener el nombre atribuido obligatorio', () => {
    const control = component.todoForm.get('title');
    control?.setValue('');
    expect(control?.valid).toBeFalsy;
  });

  it('El formulario debe tener estos dos controles', () => {
    expect(component.todoForm.contains('title')).toBeTruthy();
    expect(component.todoForm.contains('id')).toBeTruthy();
  });

  it('Undefined y Null', () => {
    expect(component.todoForm).not.toBeNull(); // que no sea null
    expect(component.todoForm).not.toBeUndefined(); // que no sea undefined
    expect(component.todoForm).toBeDefined(); // que sea defined (no sea undefined)
  });

  it('Nothing', () => {
    expect().nothing();
  });

  it('Greater/Smaller', () => {
    expect(component.todos.length).toBeGreaterThanOrEqual(20);
  });

  it('El botón hace click', () => {
    fakeAsync(() => {
      const button =
        fixture.debugElement.nativeElement.querySelector('#createButton');
      button.click();
      tick();
      expect(component.createTodo).toHaveBeenCalled();
    });
  });

  // SHALLOW Test para comprobar que el componente pinta correctamente en el HTML
  // la lista de tareas obtenida del servicio
  it('Debería renderizar la lista de tareas en el HTML', () => {
    fakeAsync(() => {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const elemento = fixture.debugElement.query(
          By.css('.todos')
        ).nativeElement;
        tick();
        expect(elemento.childNodes[1]).not.toBeNull();
      });
    });
  });
});
