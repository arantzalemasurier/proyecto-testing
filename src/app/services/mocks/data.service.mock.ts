import { Observable, of } from 'rxjs';

export class TodoServiceStub {
  static deleteTodo: any;
  getTodos(): Observable<any> {
    return of({
      data: [
        {
          id: 37,
          user_id: 25,
          title:
            'Appello appositus benigne rem tripudio sumptus comprehendo cultellus adsidue.',
          due_on: '2021-10-11T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 38,
          user_id: 26,
          title:
            'Non teneo dapifer ars laborum suscipio appello subito admoveo coepi apto.',
          due_on: '2021-10-19T00:00:00.000+05:30',
          status: 'completed',
        },
        {
          id: 39,
          user_id: 26,
          title: 'Usque corroboro apud dolor caelum cibus animi.',
          due_on: '2021-09-26T00:00:00.000+05:30',
          status: 'completed',
        },
        {
          id: 40,
          user_id: 28,
          title: 'Aut cresco accendo tenetur damnatio vivo.',
          due_on: '2021-10-14T00:00:00.000+05:30',
          status: 'completed',
        },
        {
          id: 41,
          user_id: 28,
          title: 'Demulceo tredecim utroque victus quasi spiculum spargo.',
          due_on: '2021-10-15T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 42,
          user_id: 28,
          title: 'Vestrum carbo vapulus vinculum versus.',
          due_on: '2021-10-02T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 45,
          user_id: 31,
          title:
            'Decretum video veniam umerus solvo aufero balbus libero deserunt.',
          due_on: '2021-10-12T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 46,
          user_id: 31,
          title:
            'Consequatur votum adstringo collum decerno veritas volva aranea eligendi.',
          due_on: '2021-10-20T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 57,
          user_id: 37,
          title: 'Asperiores cresco depono astrum amiculum.',
          due_on: '2021-10-10T00:00:00.000+05:30',
          status: 'completed',
        },
        {
          id: 58,
          user_id: 37,
          title:
            'Audeo carcer utrimque accusator voluptatem subiungo caries vergo vero denuncio trucido.',
          due_on: '2021-10-10T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 59,
          user_id: 39,
          title:
            'Desino compello bestia error defaeco brevis suscipio varius undique attollo accendo.',
          due_on: '2021-10-21T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 67,
          user_id: 44,
          title: 'Solitudo et solium turbo depulso vilicus demum vado.',
          due_on: '2021-10-03T00:00:00.000+05:30',
          status: 'completed',
        },
        {
          id: 68,
          user_id: 44,
          title: 'Cena creo tenuis credo pauper qui tersus.',
          due_on: '2021-09-22T00:00:00.000+05:30',
          status: 'completed',
        },
        {
          id: 70,
          user_id: 47,
          title: 'Soleo tergiversatio defessus acceptus deporto talus.',
          due_on: '2021-10-01T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 71,
          user_id: 47,
          title:
            'Amo surculus esse cena reiciendis celebrer quibusdam spectaculum demum deripio curso.',
          due_on: '2021-10-08T00:00:00.000+05:30',
          status: 'completed',
        },
        {
          id: 80,
          user_id: 53,
          title:
            'Amicitia totidem cognatus tego dolor aut voluptatem terra autem celebrer tui.',
          due_on: '2021-10-19T00:00:00.000+05:30',
          status: 'completed',
        },
        {
          id: 83,
          user_id: 57,
          title: 'Aut aestivus omnis vulticulus virga supra.',
          due_on: '2021-09-28T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 84,
          user_id: 57,
          title: 'Traho occaecati acidus inventore comprehendo.',
          due_on: '2021-10-21T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 86,
          user_id: 59,
          title: 'Agnitio canonicus sponte neque nemo apostolus quos.',
          due_on: '2021-09-30T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 87,
          user_id: 59,
          title: 'Sol aiunt exercitationem contego autem cupiditas et demum.',
          due_on: '2021-10-11T00:00:00.000+05:30',
          status: 'pending',
        },
      ],
    });
  }

  getTodo(id: number): Observable<any> {
    return of({
      data: {
        id: 87,
        user_id: 59,
        title: 'Sol aiunt exercitationem contego autem cupiditas et demum.',
        due_on: '2021-10-11T00:00:00.000+05:30',
        status: 'pending',
      },
    });
  }
}
