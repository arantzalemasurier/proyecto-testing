import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Todo } from 'src/models/Todo';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  baseUrl: string = 'https://gorest.co.in/public/v1';

  constructor(private httpClient: HttpClient) {}

  getTodos(): Observable<any> {
    return this.httpClient.get('https://gorest.co.in/public/v1/todos');
  }

  getTodo(id: number): Observable<any> {
    return this.httpClient.get(`https://gorest.co.in/public/v1/todos/${id}`);
  }
}
