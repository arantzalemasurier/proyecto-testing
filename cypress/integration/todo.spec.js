describe("Componente HTML Todo", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("Comprobar botón createTodo", () => {
    cy.get("#createButton").should("have.text", "Create");
  });
});

describe("Navegar por tareas", () => {
  it("Crear, borar o consultar una tarea", () => {
    cy.visit("/");
  });
});

describe("Prueba de crear y borrar tareas", () => {
  it("Crear tarea", () => {
    cy.visit("/").then(() => {
      cy.contains("Create").click();
      cy.get('[placeholder="Id"]').type(8000);
      cy.get('[placeholder="User ID"]').type(26);
      cy.get('[placeholder="Title"]').type("Test de crear Tarea");
      cy.get('[placeholder="Due on"]').type("2021-10-23");
    });
  });

  it("Comprueba el contenido de la tabla", () => {
    cy.visit("/").then(() => {
      cy.contains("th", "Id")
        .invoke("index")
        .then((i) => {
          console.log(i);
        });
    });
  });

  it("Borrar Tarea", () => {
    cy.visit("/").then(() => {
      cy.contains("Delete").click();
    });
  });
});
